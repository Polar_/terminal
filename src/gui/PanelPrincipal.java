package gui;

import java.awt.CardLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelPrincipal extends JPanel{
	
	private CardLayout gestorPaneles;
	
	public PanelPrincipal() {
		gestorPaneles = new CardLayout();
		setLayout(gestorPaneles);
	}
	
	public void mostrarPanel(String nombre) {
		gestorPaneles.show(this,  nombre);
	}

}
