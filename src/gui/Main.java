package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import usuario.*;

@SuppressWarnings("serial")
public class Main extends JFrame{
	
	///Uusario que ha logueado, puede ser Admin o Pasajero
	private Usuario logueado;
	
	//Gestores
	private GestorUsuario usuarios;
	
	//Paneles
	private PanelPrincipal pnPrincipal;
	private PanelLogin pnLogin;
	private PanelAdmin pnAdmin;
	
	public Main() {
		
		usuarios = new GestorUsuario();
		pnPrincipal = new PanelPrincipal();
		pnLogin = new PanelLogin();
		pnLogin.setAccionBotonLogin(new AccionLogin());
		pnAdmin = new PanelAdmin();
		pnAdmin.setAccionLogout(new AccionLogout());
		
		//Añadimos cada panel al panel principal.
		pnPrincipal.add(pnLogin, "login");
		pnPrincipal.add(pnAdmin, "admin");
		
		setContentPane(pnPrincipal);
		
		setTitle("Gestion de Terminales");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	//Acciones
	/**
	 * Recoge los datos del formulario de login y permite acceder
	 * a un usuario registrado, o bien crear un nuevo registro.<br>
	 * Cuando el Usuario ha accedido al sistema, se mostrará un panel
	 * de opciones según si es Administrador o Pasajero.
	 */
	private class AccionLogin implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			//Recuperamos datos del formulario login
			String[] datosLogin = pnLogin.getDatos();
			
			if (datosLogin !=  null) {
				//Preguntamos si es nuevo registro
				if (pnLogin.esNuevoRegistro()) {
					//TODO: Codigo para crear nuevo registro
				}
				else {//Es un login normal
					//Intentamos login con los datos
					logueado = usuarios.login(datosLogin[0], datosLogin[1]);
				}
				
				//Comprobamos si ha tenido exito el login y que tipo usuario se trata
				if (logueado == null)
					JOptionPane.showMessageDialog(null,
							"Los datos introducidos no coinciden con ningun Usuario",
							"Login Usuario", JOptionPane.WARNING_MESSAGE);
				else {
					//Login exitoso. Se mostrará un nuevo panel, según tipo usuario
					if (logueado instanceof Administrador)
						pnPrincipal.mostrarPanel("admin");
					else
						pnPrincipal.mostrarPanel("pasajero");
					//El formulario de login se resetea
					pnLogin.resetFormulario();
				}
			}
		}
	}
	
	private class AccionLogout implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			logueado = null;
			pnPrincipal.mostrarPanel("login");
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Main();	
			}		
		});
	}
}