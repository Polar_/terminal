package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelAdmin extends JPanel{
	
	private BotonOpcion btTerminal;
	private BotonOpcion btUnidad;
	private BotonOpcion btRuta;
	private BotonOpcion btReporte;
	private JButton btLogout;
	
	public PanelAdmin() {
		
		btTerminal = new BotonOpcion("Gestión<br>de<br>Terminales");
		btUnidad = new BotonOpcion("Gestión<br>de<br>Unidades");
		btRuta = new BotonOpcion("Gestión<br>de<br>Rutas");
		btReporte = new BotonOpcion("Reportes");
		btLogout = new JButton("Cerrar Sesión");
		
		setLayout(new BorderLayout());
		add(new PanelNorte(), BorderLayout.NORTH);
		add(new PanelCentral(), BorderLayout.CENTER);
		add(new PanelSur(), BorderLayout.SOUTH);
		
	}
	
	private class PanelNorte extends JPanel {
		
		public PanelNorte() {
			JLabel titulo = new JLabel("Panel de Administrador");
			titulo.setFont(new Font("Verdana", Font.ITALIC, 36));
			add(titulo);
			setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createEmptyBorder(20, 20, 0, 20),
					BorderFactory.createRaisedSoftBevelBorder()));
		}
	}
	
	private class PanelCentral extends JPanel {
		
		public PanelCentral() {
			
			setLayout(new GridLayout(2,2,15,15));
			add(btTerminal); add(btUnidad);
			add(btRuta); add(btReporte);
			setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		}
	}
	
	private class PanelSur extends JPanel {
		
		public PanelSur() {
			JPanel pnBoton = new JPanel();
			pnBoton.add(btLogout);
			add(pnBoton);
		}
	}
	
	private class BotonOpcion extends JButton {
		
		public BotonOpcion(String texto) {
			super("<html><p style=\"text-align:center\">" + texto + "</p></html>");
			setFont(new Font("Verdana", Font.BOLD, 18));
			setFocusPainted(false);
			setBackground(new Color(102, 204, 255));
			setForeground(Color.WHITE);
			setBorder(BorderFactory.createCompoundBorder(
					BorderFactory.createEtchedBorder(),
					BorderFactory.createEmptyBorder(10, 10, 10, 10)));
			addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseReleased(MouseEvent e) {}
				@Override
				public void mouseEntered(MouseEvent e) {
					setForeground(new Color(204, 0, 0));
				}
				@Override
				public void mouseExited(MouseEvent e) {
					setForeground(Color.WHITE);
				}
			});
		}
	}
	
	//Métodos
	public void setAccionLogout(ActionListener accion) {
		btLogout.addActionListener(accion);
	}}