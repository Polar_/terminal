package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PanelLogin extends JPanel{
	
	private JTextField jtCedula;
	private JPasswordField jtPassword;
	private JCheckBox jcNuevoRegistro;
	private JButton btLogin;
	
	public PanelLogin() {
		jtCedula = new JTextField(8);
		jtPassword = new JPasswordField(8);
		jcNuevoRegistro = new JCheckBox("Nuevo Registro");
		jcNuevoRegistro.setToolTipText("Marque esta casilla para registrar un nuevo usuario");
		btLogin = new JButton("Iniciar Sesión");
		
		setLayout(new BorderLayout());
		add(new PanelCentral(), BorderLayout.CENTER);
		add(new PanelSur(), BorderLayout.SOUTH);
		
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	}
	
	private class PanelCentral extends JPanel {
		
		public PanelCentral() {
			
			JLabel texto = new JLabel("Introduzca datos para iniciar sesión");
			texto.setFont(new Font("Verdana", Font.BOLD, 24));
			JPanel pnTexto = new JPanel();
			pnTexto.add(texto);
			
			JPanel pnNombre = new JPanel();
			pnNombre.add(new PanelCampoTexto("Cédula: ", jtCedula));
			
			JPanel pnPass = new JPanel();
			pnPass.add(new PanelCampoTexto("Password: ", jtPassword));
			
			JPanel pnRegistro = new JPanel();
			pnRegistro.add(jcNuevoRegistro);
			
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			add(pnTexto); add(pnNombre); add(pnPass); add(pnRegistro);
		}
	}
	
	private class PanelSur extends JPanel {
		
		public PanelSur() {
			JPanel pnBoton = new JPanel();
			pnBoton.add(btLogin);
			add(pnBoton);
		}
	}
	
	private class PanelCampoTexto extends JPanel {
		
		public PanelCampoTexto(String texto, JComponent campo) {
			setLayout(new FlowLayout(FlowLayout.RIGHT));
			add(new JLabel(texto));
			add(campo);
		}
	}
	
	//Métodos
	/**
	 * Retorna los datos del formulario introducidos por el usuario.<br>
	 * Si algún campo esta vacío, se avisa en pantalla y se retorna null.<br>
	 * Si ambos campos contienen datos, se retornan juntos en un array.
	 * @return Array de String[] con el valor de la Cédula y el Password
	 */
	public String[] getDatos() {
		String cedula = jtCedula.getText();
		String password = String.valueOf(jtPassword.getPassword());
		//Comprobamos si hay datos
		if (cedula.isEmpty()) {
			JOptionPane.showMessageDialog(null, "El campo Cédula no puede estar vacío",
					"Login Usuario", JOptionPane.WARNING_MESSAGE);
			return null;
		} else if (password.isEmpty()) {
			JOptionPane.showMessageDialog(null, "El campo Password no puede estar vacío",
					"Login Usuario", JOptionPane.WARNING_MESSAGE);
			return null;
		} else {
			//Juntamos ambos datos en un array y lo retornamos
			return new String[] {cedula, password};
		}
	}
	
	/**
	 * Indica si el Usuario está solicitando crear un nuevo registro
	 * con los datos que ha introducido.<br>El Usuario puede hacer esta
	 * solicitud marcando el CheckBox correspondiente.
	 * @return TRUE si es nuevo registro, FALSE en caso contrario.
	 */
	public boolean esNuevoRegistro() {
		return jcNuevoRegistro.isSelected();
	}
	
	/**
	 * Resetea el formulario borrando los valores
	 * de todos sus campos.
	 */
	public void resetFormulario() {
		jtCedula.setText(null);
		jtPassword.setText(null);
		jcNuevoRegistro.setSelected(false);
	}
	
	/**
	 * Recibe un ActionListener para agregarlo al botón de accion.<br>
	 * Este Listener se escribe en la clase Main porque requiere poder
	 * interactuar con otros objetos como el "Gestor de Usuarios" y
	 * el "Panel Principal" de la interfaz, que se encuentran fuera
	 * de este ámbito.
	 * @param accionBotonLogin ActionListener para el botón de acción
	 */
	public void setAccionBotonLogin(ActionListener accionBotonLogin) {
		btLogin.addActionListener(accionBotonLogin);
	}

}

