/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

/**
 *
 * @author Nikki
 */
public abstract class Usuario {
    protected String cedula;
	protected String password;
	
	public Usuario(String cedula, String password) {
		this.cedula = cedula;
		this.password = password;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Dos Usuarios son iguales si tienen
	 * la misma cédula.
	 */
	@Override
	public boolean equals(Object objeto) {
		if (objeto instanceof Usuario) {
			Usuario otroUsu = (Usuario) objeto;
			return cedula.equals(otroUsu.cedula);
		}
		else
			return false;
	}
    
}
