/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikki
 */
public final class GestorUsuario {
    private ArrayList<Usuario> usuarios;
	
	public GestorUsuario() {
		usuarios = new ArrayList<Usuario>();
		cargarDatos();
	}
	
	public Usuario login(String cedula, String password) {
		//Buscará un usuario que coincida cedula y password, y lo retornará
		for (Usuario usu: usuarios) {
			if (usu.getCedula().equals(cedula) && usu.getPassword().equals(password))
				return usu;
		}
		//Si bucle for no ha retornado ningun usuario, es que el login no es válido
		return null;
	}
	
	/**
	 * Leerá el archivo de datos de Usuarios.<br>
	 * En caso de no existir archivo, o fracasar la lectura,
	 * creará automáticamente un usuario Administrador por defecto.
	 */
	private void cargarDatos() {
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"));
			String linea = br.readLine();
			
			while(linea != null) {
				
				//Separamos datos contenidos en la linea
				String[] datos = linea.split(" -- ");
				
				if (datos[0].equals("A")) {//Este usuario es un Administrador
					//Administrador solo tiene cedula y password
					usuarios.add(new Administrador(datos[1], datos[2]));	
				} else if (datos[0].equals("P")) { //Pasajero
					//La fecha nacim. esta guardada en milisegundos,
					//hay que generar un Date con ellos.
					long milis = Long.parseLong(datos[4]);
					Date fechaNac = new Date(milis);
					usuarios.add(new Pasajero(datos[1], datos[2], datos[3], fechaNac, datos[5], datos[6]));
				}
				
				//siguiente linea
				linea = br.readLine();
			}
			
			br.close();
			
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "No se encuentra archivo: usuarios.txt\n"
					+ "Se intentará generar uno nuevo", "Cargar Datos Usuarios",
					JOptionPane.WARNING_MESSAGE);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Error intentando leer: usuarios.txt",
					"Cargar Datos Usuarios", JOptionPane.WARNING_MESSAGE);
		} catch (Exception error) {
			JOptionPane.showMessageDialog(null, "Error recuperando datos de: usuarios.txt\n"
					+ "Error:\n" + error.getLocalizedMessage(),
					"Cargar Datos Usuarios", JOptionPane.WARNING_MESSAGE);
		}
		
		//Si fracasa la lectura de Usuarios, creamos nuevo archivo con un admin por defecto
		if (usuarios.isEmpty()) {
			usuarios.add(new Administrador("CED000", "admin"));
			guardarDatos();
		}
		
	}
	
	/**
	 * Guardará los datos de Usuarios en un archivo de texto.
	 */
	private void guardarDatos() {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("usuarios.txt", false));
			//Por cada usuario, creamos una línea de texto en el archivo
			//Será necesario, hacer casting a cada tipo de Usuarios
			for (Usuario usu: usuarios) {
				if (usu instanceof Administrador)
					bw.write(((Administrador)(usu)).toString());
				else
					bw.write(((Pasajero)(usu)).toString());
				
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error intentando guardar: usuarios.txt",
					"Guardar Datos Usuarios", JOptionPane.WARNING_MESSAGE);
		}
	}
}
