/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

/**
 *
 * @author Nikki
 */
public final class Administrador extends Usuario {
    
	public Administrador(String cedula, String password) {
		super(cedula, password);
	}
	
	/**
	 * Devuelve los valores de los atributos con la letra A inicial
	 * para identificar al Usuario como un Administrador a la hora de
	 * recuperar los datos del archivo txt.
	 */
	@Override
	public String toString() {
		return String.format("A -- %s -- %s", cedula, password);
	}
}
