/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import java.util.Date;

/**
 *
 * @author Nikki
 */
public final class Pasajero extends Usuario {

    private String nombre;
    private Date fechaNac;
    private String genero;
    private String eMail;

    public Pasajero(String cedula, String password, String nombre,
            Date fechaNac, String genero, String eMail) {
        super(cedula, password);
        this.nombre = nombre;
        this.fechaNac = fechaNac;
        this.genero = genero;
        this.eMail = eMail;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    /**
     * Devuelve los valores de los atributos con la letra P inicial para
     * identificar al Usuario como un Pasajero a la hora de recuperar los datos
     * del archivo txt.<br>
     * La fecha, un objeto Date, se guarda como milisegundos.
     */
    @Override
    public String toString() {
        return String.format("P -- %s -- %s -- %s -- %d -- %s -- %s",
                cedula, password, nombre, fechaNac.getTime(), genero, eMail);
    }
}
